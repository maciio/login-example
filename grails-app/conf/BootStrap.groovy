import com.coface.paula.app.Role
import com.coface.paula.app.User
import com.coface.paula.app.UserRole

class BootStrap {

    def init = { servletContext ->
        if (!User.count) {

        }
        def adminRole = new Role(authority: 'ROLE_ADMIN').save(flush: true)
        def userRole = new Role(authority: 'ROLE_USER').save(flush: true)

        def user1 = new User(
                username: 'admin',
                password: 'adminadmin').save(flush: true)
        def user2 = new User(
                username: 'pau',
                password: 'mistika1512').save(flush: true)

        UserRole.create(user1, adminRole, true)
        UserRole.create(user2, userRole, true)
    }
    def destroy = {
    }
}
