modules = {
    application {
        resource url:'js/application.js'
    }
    jquery {
        resource url: 'js/jquery.min.js', disposition: 'head'
        resource url: 'js/jquery-ui.min.js', disposition: 'head'
    }
    core {
        dependsOn 'jquery'
        resource url: 'css/bootstrap.min.css', disposition: 'head'
        resource url: 'css/ionicons.min.css', disposition: 'head'
        resource url: 'css/morris/morris.css', disposition: 'head'
        resource url: 'css/jvectormap/jquery-jvectormap-1.2.2.css', disposition: 'head'
        resource url: 'css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css', disposition: 'head'
        resource url: 'css/AdminLTE.css'

        resource url: 'js/html5.js', wrapper: { s -> "<!--[if lt IE 9]> ${s} <![endif]-->" }, disposition: 'head'
        resource url: 'js/respond.min.js', wrapper: { s -> "<!--[if lt IE 9]> ${s} <![endif]-->" }, disposition: 'head'
        resource url: 'js/moderniz.js'
    }
}