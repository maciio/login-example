<html class="bg-black" style="height: 100%">
<head>
    <meta name='layout' content='main'/>
    <title><g:message code="springSecurity.login.title"/></title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <r:require module="core"/>
</head>

<body>

<body class="bg-black">

<div class="form-box" id="login-box">
    <div class="header">Sign In</div>

    <form action='${postUrl}' method='POST'>
        <div class="body bg-gray">
            <div class="form-group">
                <input type="text" name="j_username" class="form-control" placeholder="${g.message(code: 'springSecurity.login.username.label')}" required />
            </div>

            <div class="form-group">
                <input type="password" name="j_password" class="form-control" placeholder="${g.message(code: 'springSecurity.login.password.label')}" required />
            </div>

            <div class="form-group">
                <input type='checkbox' class='chk' name='${rememberMeParameter}' id='remember_me' <g:if test='${hasCookie}'>checked='checked'</g:if>/>
                <label for='remember_me'><g:message code="springSecurity.login.remember.me.label"/></label>
            </div>
        </div>

        <div class="footer">
            <button type="submit" class="btn bg-olive btn-block">Sign me in</button>

            <p><a href="#">I forgot my password</a></p>
        </div>
    </form>
</div>

</html>
